<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use DB;
use Carbon\Carbon;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            DB::beginTransaction();
            $user = User::create([
                'name'=>'Jamil Admin',
                'email'=>'jamil@admin.com',
                'password'=>\Hash::make('12345678'),
                'email_verified_at'=>Carbon::now(),
            ]);
            $user->assignRole('superadmin');
            DB::commit();
            $this->command->info('super admin berhasil dimasukkan');
            
        } catch (\Throwable $th) {
            DB::rollback();
            $this->command->info('super admin gagal dimasukkan');
        }
    }
}
