<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\user;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $jumlah = 20;
        User::factory($jumlah)->create();
        $this->command->info('berhasil menambahkan '.$jumlah. ' pengguna baru');
    }
}
