<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        return view('homepage.index')->with(['type_menu'=>'dashboard']);
    }
}
