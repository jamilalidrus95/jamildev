<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,Session;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function proses(Request $request){
       
        $request->validate([
            'email'=>'required|email',
            'password'=>'required',
        ],[
            'email.required'=>'email tidak boleh kosong',
            'email.email'=>'email tidak valid',
            'password.required'=>'password tidak boleh kosong',
        ]);
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
           return redirect()->route('home');
        }else{
            return redirect()->back()->withErrors('email dan password salah')->withInput($request->all());
        }
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect()->route('login');
    }
}
