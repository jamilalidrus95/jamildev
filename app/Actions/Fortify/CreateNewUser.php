<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Illuminate\Auth\Notifications\VerifyEmail;
use DB;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     */
    public function create(array $input): User
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ],[
            'name.required'=>'nama tidak boleh kosong',
            'email.required'=>'email tidak boleh kosong',
            'password.required'=>'password tidak boleh kosong',
            'password.confirmed'=>'password confirmation tidak sama',
        ])->validate();
        
        DB::beginTransaction();
        try {
            $user = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
            ]);
            $user->assignRole('user');
            DB::commit();
            return $user;
        } catch (\Throwable $th) {
            DB::rollBack();
            return false;
        }

    }
}
