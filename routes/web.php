<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::get('register',function(){
    return view('register.register');
})->name('register');


Route::get('login',[loginController::class,'index'])->name('login');
Route::post('login',[loginController::class,'proses'])->name('login');
Route::group(['middleware'=>['auth','role:superadmin|user','verified']],function(){
    Route::get('logout',[LoginController::class, 'logout'])->name('logout');
    Route::get('home',[DashboardController::class,'index'])->name('home');
});

Route::group(['middleware'=>['auth','role:superadmin','verified']],function(){
    Route::resource('manage-user',UserController::class);
});

