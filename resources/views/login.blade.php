@extends('layouts.auth')

@section('title', 'Login')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('library/bootstrap-social/bootstrap-social.css') }}">
@endpush

@section('main')
<div class="card card-primary">
    <div class="card-header">
        <h4>Login</h4>
    </div>

    <div class="card-body">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route'=>'login','method'=>'POST','class'=>'needs-validation']) !!}
        @csrf
        <div class="form-group">
            <label for="email">Email</label>
           {!! Form::email('email',null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <div class="d-block">
                <label for="password" class="control-label">Password</label>
                <div class="float-right">
                    <a href="{{ route('password.request') }}" class="text-small">
                        Forgot Password?
                    </a>
                </div>
            </div>
           {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                Login
            </button>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<div class="text-muted mt-5 text-center">
    Don't have an account? <a href="{{ route('register') }}">Create One</a>
</div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
