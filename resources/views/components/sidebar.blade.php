<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">
                <img style="max-width:35%" src="{{ asset('img/jamildevlogo.png') }}" alt="">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">JD</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item  {{ Request::is('home*') ? 'active' : '' }}">
                <a href="#"
                    class="nav-link"><i class="fas fa-home"></i><span>Beranda</span></a>
            </li>
            <li class="menu-header">Starter</li>
            @role('superadmin')
            <li class="nav-item dropdown {{ Request::is('manage-user*') ? 'active' : '' }}">
                <a href="#"
                    class="nav-link has-dropdown"
                    data-toggle="dropdown"><i class="fas fa-user-group"></i> <span>Manajemen Pengguna</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('layout-default-layout') ? 'active' : '' }}">
                        <a class="nav-link"
                            href="{{ route('manage-user.index') }}">Daftar Pengguna</a>
                    </li>
                </ul>
            </li>
            @endrole
        </ul>

        {{-- <div class="hide-sidebar-mini mt-4 mb-4 p-3">
            <a href="https://getstisla.com/docs"
                class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
            </a>
        </div> --}}
    </aside>
</div>
