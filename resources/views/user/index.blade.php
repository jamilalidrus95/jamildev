@extends('layouts.master')

@section('title', 'Posts')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('library/selectric/public/selectric.css') }}">
    <style>
        .form-margin{
            margin-left:10px !important;
        }
    </style>
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ $title }}</h1>

            </div>
            <div class="section-body">
                <h2 class="section-title">Semua Users</h2>
                <p class="section-lead">
                    Anda dapat mengatur semua data pengguna seperti menambah, mengubah dan menghapus
                </p>


                <div class="row mt-4">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>All Users</h4>
                            </div>
                            <div class="card-body">

                                <div class="float-right">
                                    {!! Form::open(['method'=>'GET','url'=>url()->current()]) !!}
                                    <div class="input-group">
                                        {!! Form::text('name',Request::get('name'),['class'=>'form-control','placeholder'=>'name']) !!}
                                        {!! Form::text('email',Request::get('email'),['class'=>'form-control form-margin','placeholder'=>'email']) !!}
                                        {!! Form::text('phone',Request::get('phone'),['class'=>'form-control form-margin','placeholder'=>'phone']) !!}
                                        <div class="input-group-append">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            @if(Request::get('name') or Request::get('email') or Request::get('phone') )
                                            <a href="{{ route('manage-user.index') }}" class="btn btn-warning"><i class="fas fa-times"></i> Reset</a>
                                            @endif
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="clearfix mb-3"></div>

                                <div class="table-responsive">
                                    <table class="table-striped table">
                                        <tr>

                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Status</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        @forelse ($users as $index => $user)
                                            <tr>

                                                <td>
                                                    {{ $index + $users->firstItem() }}
                                                </td>
                                                <td>
                                                    {{ $user->name }}
                                                </td>
                                                <td>
                                                    {{ $user->email }}
                                                </td>
                                                <td>{{ $user->phone }}</td>
                                                <td>
                                                    <div class="badge {{ $user->email_verified_at != null ? 'badge-primary' : 'badge-light' }}">
                                                        @if ($user->email_verified_at != null)
                                                            Active
                                                        @else
                                                            Pending
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="badge {{ $user->roles->first()->name == 'user' ? 'badge-light' : 'badge-dark' }}">
                                                        {{ $user->roles->first()->name }}

                                                    </div>
                                                </td>
                                                <td>
                                                    {!! Form::open(['route'=>['manage-user.destroy',$user->id],'method'=>'delete']) !!}    
                                                        <a href="" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                                        <a href="" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                                    {!! Form::close() !!}    
                                                </td>
                                            </tr>
                                        @empty
                                        <td colspan="6">
                                            <center>
                                                Tidak ada hasil
                                            </center>
                                        </td>
                                        @endforelse


                                    </table>
                                </div>
                                <div class="float-right">
                                    <nav>
                                        <ul class="pagination">
                                            {{ $users->withQueryString()->links() }}
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/features-posts.js') }}"></script>
@endpush
