@extends('layouts.auth')

@section('title', 'Register')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('library/selectric/public/selectric.css') }}">
@endpush

@section('main')
<div class="card card-primary">
    <div class="card-header">
        <h4>Register</h4>
    </div>
    <div class="card-body">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(['method'=>'POST']) !!}
        <div class="row">
            <div class="form-group col-12">
                <label for="frist_name">Name</label>
                {!! Form::text('name',null,['class'=>'form-control']) !!}

            </div>
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            {!! Form::email('email',null,['class'=>'form-control']) !!}
        </div>

        <div class="row">
            <div class="form-group col-12">
                <label for="password" class="d-block">Password</label>
                {!! Form::password('password',['class'=>'form-control']) !!}
            </div>
            <div class="form-group col-12">
                <label for="password2" class="d-block">Password Confirmation</label>
                {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
            </div>
        </div>



        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg btn-block">
                Register
            </button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('library/selectric/public/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('library/jquery.pwstrength/jquery.pwstrength.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('js/page/auth-register.js') }}"></script>
@endpush
